import sys
import argparse

def fibs(n):
    """
    input is a int
    """
    result = [0, 1]
    for i in range(n-2):
        result.append(result[-2] + result[-1])
        
    return result


    
    
if __name__=="__main__":
    
    # method 1:
    
#     print("This is the name of the script: {}" .format(sys.argv[0]))
#     print("Number of arguments: {}".format(len(sys.argv)))
#     print("The arguments are: {}".format(str(sys.argv)))
        
#     input_num = int(sys.argv[1])
    
#     lst = fibs(input_num)
#     print(lst)

    # method 2:
    # Required positional argument
    parser = argparse.ArgumentParser(description='this function is for calculate fib')
    # Add arguments
    parser.add_argument("-n", "--number_1", required=True, type=int,
                        help="number for fibs function")
    parser.add_argument("-v", "--var_1", required=False,
                        help="variable for fibs function")
    args = parser.parse_args() # Namespace
    num = vars(parser.parse_args()) # convert to dic
    
    print(args)
    print(args.number_1)
    print(num)
    print(num['number_1'])
    
    
    input_num = num['number_1']
    
    lst = fibs(input_num)
    print(lst)

