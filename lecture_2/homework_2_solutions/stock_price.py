from pandas_datareader import data as wb
from datetime import datetime, timedelta, date
import time
import os


def deco_timer(func):
    def wrapper(*args, **kwargs):
        startTime = time.time()
        func(*args, **kwargs)
        endTime = time.time()
        msecs = (endTime - startTime) * 1000
        print("time is %d ms" % msecs)

    return wrapper


class get_stock:

    def __init__(self, start_date, end_date, **kwargs):
        """

        :param kwargs: values for stock name
        """
        self.start_date = self.date_transfer(start_date)
        self.end_date = self.date_transfer(end_date)
        self.stock_list = kwargs
        self.stock_names = list(self.stock_list.values())
        print(self.start_date)
        print(self.end_date)
        print(self.stock_names)

        self.current_dir = os.getcwd()
        print("The current working directory is %s" % self.current_dir)

    def date_transfer(self, date_str):
        """
        date format
        :return:
        """
        format_str = "%Y-%m-%d"
        date_datetime = datetime.strptime(date_str, format_str)
        return date_datetime

    def get_stock_price(self, stock_name):
        """
        Main function
        :return:
        """
        df_price = wb.get_data_yahoo([stock_name], start=self.start_date, end=self.end_date)
        return df_price[['Adj Close', 'Open']]

    def create_dir(self, directory):
        # # Method 1
        # try:
        #     os.mkdir(path)
        # except OSError:
        #     print("Creation of the directory %s failed" % path)
        # else:
        #     print("Successfully created the directory %s " % path)
        if not os.path.exists(directory):
            os.makedirs(directory)
        else:
            print(" Dir already exist: {}".format(directory))

    def save_tofiles(self, df_price, stock_name):
        desti_date_list = df_price.index.to_list()
        for e, desti_date in enumerate(desti_date_list):
            # prepare path

            path_name = self.current_dir + '/outputs/' + str(desti_date.year) + '/' + str(desti_date.month) + '/' + str(desti_date.day)

            # split dataframe for daily use:
            df_daily = df_price.loc[df_price.index[e]].to_frame()

            # create new dir if not exist
            self.create_dir(path_name)

            # write to file:
            file_name = stock_name + '-' + str(desti_date.year) + '-' + str(desti_date.month) + '-' + str(desti_date.day) + '.csv'
            destination_path = path_name + '/' + file_name
            print(destination_path)
            df_daily.to_csv(destination_path)


    @deco_timer
    def trigger_func(self):

        for stock_name in self.stock_names:
            print("====> getting stock price for: {}".format(stock_name))
            df_price = self.get_stock_price(stock_name)
            print("====> Splitting the dataframe and write to each folders")
            self.save_tofiles(df_price, stock_name)

        print("$$$$$ Done $$$")


if __name__ == "__main__":
    start_date = "2020-01-01"
    end_date = "2020-01-30"
    run_class = get_stock(start_date,
                          end_date,
                          stock_1="CTC-A.TO",
                          stock_2="SHOP",
                          stock_3="AAPL")

    run_class.trigger_func()
