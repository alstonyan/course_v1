#!/usr/bin/env bash
# chmod +x /home/alston_yan_project_a/course_v1/lecture_3/cronjob
# */1 * * * * /home/alston_yan_project_a/course_v1/lecture_3/cronjob/cronjob_test.sh

today=$(date '+%Y%m%d')

logfile="/home/alston_yan_project_a/course_v1/lecture_3/cronjob/log/log-$today.txt"

python /home/alston_yan_project_a/course_v1/lecture_3/cronjob/python_printtime.py >> $logfile 2>&1 
