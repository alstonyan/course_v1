#!/usr/bin/env bash

today=$(date '+%Y%m%d')
logfile="/log/indeed-log-$today.txt"

python /indeed_script/step1_collect_data.py >> $logfile 2>&1 \
&& python /indeed_script/step2_store_data.py >> $logfile 2>&1 \
&& python /indeed_script/step3_manipulate_data.py >> $logfile 2>&1 \
&& python /indeed_script/step4_send_email.py >> $logfile 2>&1