import os
import pandas as pd
from datetime import date


def read_from_csv():
    print("---> Read file to pandas")
    current_workdir = os.path.abspath(os.path.join(os.getcwd(), "."))
    file_dir = current_workdir + "/file/output_raw.csv"

    df = pd.read_csv(file_dir, header=0)

    return df


def to_gbq(df):
    print("---> Write to gbq")
    today_date = date.today()
    sdt = today_date.strftime("%Y%m%d")

    table_name_list = ["indeed_data", sdt]
    table_name = '_'.join(table_name_list).strip()

    df.to_gbq(destination_table='indeed_api.{}'.format(table_name),
              project_id='alston-project-1', if_exists='replace', verbose=True)


def main():
    df = read_from_csv()
    to_gbq(df)


if __name__ == "__main__":
    main()
