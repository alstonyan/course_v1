from airflow import DAG
from airflow.operators.python_operator import PythonOperator
from airflow.operators.dummy_operator import DummyOperator
from airflow.contrib.sensors.file_sensor import FileSensor
from airflow.operators.bash_operator import BashOperator
from datetime import timedelta, date, datetime
    
# This is airflow arg area:
default_args = {
    "owner": "alston",
    "depends_on_past": False,
    "start_date": datetime(2020, 8, 14),
    "retries": 1,
    'retry_delay': timedelta(minutes=5)
}

dag = DAG(dag_id='my_first_dag', 
          description='Simple tutorial DAG',
          default_args=default_args,
          schedule_interval='0 12 * * *', 
          catchup=False)

# This is python code area:
def print_hello():
    return 'Hello world!'



dummy_operator = DummyOperator(task_id='dummy_task', 
                               retries=3, 
                               dag=dag)

hello_operator = PythonOperator(task_id='hello_task', 
                                python_callable=print_hello, 
                                dag=dag)

waiting_for_files = FileSensor(task_id='waiting_for_files', 
                               fs_conn_id='fs_demo', 
                               filepath="data.csv", 
                               poke_interval=30,
                               dag=dag)

demo_bash = BashOperator(task_id='demo_bash',
                        bash_command="""
                        bash /home/alston_yan_project_a/course_v1/lecture_3/cronjob/cronjob_test.sh
                        """,
                        dag=dag)

# dummy_operator >> hello_operator
# hello_operator.set_upstream(dummy_operator)

# check dags: airflow list_dags
# check tasks: airflow list_tasks my_first_dag
# test: airflow test my_first_dag waiting_for_files 2020-08-14

dummy_operator >> waiting_for_files >> hello_operator >> demo_bash