#  aggregate delay examples:
SELECT
  ORIGIN,
  AVG(DEP_DELAY) as arr_delay,
  AVG(ARR_DELAY) as dep_delay
FROM
  `flights.tzcorr`
GROUP BY
  ORIGIN


# complete dataset that contains a full year's worth of data
SELECT
  *
FROM (
  SELECT
    ORIGIN,
    AVG(DEP_DELAY) AS dep_delay,
    AVG(ARR_DELAY) AS arr_delay,
    COUNT(ARR_DELAY) AS num_flights
  FROM
    `flights.tzcorr`
  GROUP BY
    ORIGIN )
WHERE
  num_flights > 3650
ORDER BY
  dep_delay DESC

 # Click the Execution Details tab. 
 # Here you'll see analytical detail on 
 # the performance of the various 
 # stages during the execution of the query.



 SELECT
  ORIGIN,
  AVG(DEP_DELAY) AS dep_delay,
  AVG(ARR_DELAY) AS arr_delay,
  COUNT(ARR_DELAY) AS num_flights
FROM
  `flights.tzcorr`
GROUP BY
  ORIGIN
HAVING
  num_flights > 3650
ORDER BY
  dep_delay DESC



  CREATE OR REPLACE TABLE flights.trainday AS
        SELECT
    FL_DATE,
    IF(ABS(MOD(FARM_FINGERPRINT(CAST(FL_DATE AS STRING)), 100)) < 70, 'True', 'False') AS is_train_day
  FROM (
    SELECT
      DISTINCT(FL_DATE) AS FL_DATE
    FROM
      `flights.tzcorr`)
  ORDER BY
    FL_DATE


  